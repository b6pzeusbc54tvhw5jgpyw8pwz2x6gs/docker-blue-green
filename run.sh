
docker stop alfreduc-manager

docker rm alfreduc-manager

mkdir data

docker run --name alfreduc-manager -d \
	--volume /var/run/docker.sock:/var/run/docker.sock:ro \
	--volume $PWD:/root/app \
	--expose 80 \
	--env PORT=80 \
	--env VIRTUAL_HOST=blue-green-manager.alfreduc.com \
	--env HOST_DATA_PATH=$PWD/data \
    b6pzeusbc54tvhw5jgpyw8pwz2x6gs/ubuntu16-for-gitlab-runner:d170123.1 \
	/bin/bash -c "cd /root/app && npm run start"
