module.exports = {

  "parser": "babel-eslint",
  "ecmaFeatures": {
    "ecmaVersion": 6,
    "templateStrings": true,
    "modules": true,
    "classes": true,
    "arrowFunctions": true,
    "blockBindings": true,
		"jsx": true
  },
  "parserOptions": {
    "sourceType": "module"
  },
	"plugins": ["babel","react"],

  "rules": {
    "semi": 1,
		"no-mixed-spaces-and-tabs": 1,
    "no-cond-assign": 1,
    "no-unreachable": 1,
    "block-scoped-var": 2,
    "no-redeclare": 2,
    "no-undef": 2,
    "no-shadow": [2, {"hoist": "functions", "allow": ["e"] }],
    "no-unused-vars": [1, {"vars": "all", "args": "none"}],
		"no-duplicate-imports": [2, { "includeExports": true }],
		"no-const-assign": 2,
    //"no-use-before-define": [2, { "functions": false, "classes": true }],

		"react/jsx-uses-react": 2,
    "react/jsx-uses-vars": 2,
    "react/jsx-no-undef": 2
  },
  "env": {
    "node": true,
    "es6": true
  },
  "globals": {
    "__PRODUCTION__": true,
    "__DEVELOPMENT__": true
  }
};

