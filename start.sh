GIT_REPO=https://gitlab.com/b6pzeusbc54tvhw5jgpyw8pwz2x6gs/alfred-uc.git
LIVE_VIRTUAL_HOST=alfreduc.com
STG_VIRTUAL_HOST=stg.alfreduc.com
VIRTUAL_PORT=80
DOCKER_IMAGE=b6pzeusbc54tvhw5jgpyw8pwz2x6gs/docker-node-v4-with-vim:4.6
CONTAINER_NAME_PREFIX=alfreduc
