
import superagent from 'superagent';
import when from 'when';
const Promise = when.Promise;

const gitlabAPI = ( api ) => new Promise( (resolve, reject)=>{

  const apiUrl = 'https://gitlab.com/api/v3';
  const token = void 0;
  const url = `${apiUrl}${api}`;
  console.log( api, url, token );

  //.set('PRIVATE-TOKEN', '9WjXpS_D4Vp1dK4okz9x' )

  return superagent.get( url )
  .set('PRIVATE-TOKEN', token )
  .type('json')
  .end( (err, res)=>{
	//console.log( res );
    if( err ) {
      console.error( err );
      reject( err );
      return;
    }
    resolve( res.body );
  });
});

console.log('hi');
gitlabAPI( '/projects');
