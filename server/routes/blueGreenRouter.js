import * as blueGreen from '../controller/blueGreen';
blueGreen.init();
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {

  res.send(
`
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'/>
    <link rel="stylesheet" href="style.css" />
    <link rel="shortcut icon" href="/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
  </head>
  <body>
    <div id="root"></div>
    <script src="bundle.js"></script>
  </body>
</html>
`);
  //res.send( blueGreen.getInfo() );
});

// switch
router.post('/switch', ( req, res )=>{
  blueGreen.switchLive();
  res.send( blueGreen.getInfo() );
});

router.post('/getInfo', ( req, res )=>{
  res.send( blueGreen.getInfo() );
});

router.post('/webhook', (req, res, next)=>{

  console.log( req.headers["x-gitlab-token"] );
  console.log( req.body );
  console.log('build_name: ' + req.body.build_name );
  console.log('build_status: ' + req.body.build_status );
  if( req.body.object_kind !== 'build' || req.body.build_status !== 'success' || req.body.build_name !== 'webhook' ) {
    res.send('ignore req');
    return;
  }

  //console.log( req.body );
  if( req.body.project_name === 'wsc / gimozzi-server' ) {
    res.send('server will be deploy');
    blueGreen.startServerToStg( req.body.sha );
  }
});

router.post('/deployToSTG', (req, res, next)=>{
  const hasError = blueGreen.startServerToStg( req.body.sha );
  if( ! hasError ) {
    res.send('server will be deploy');
  } else {
    res.send('server is busy');
  }
});

module.exports = router;
