import execSync from 'exec-sync-uc';
import fs from 'fs';
import path from 'path';
import _ from 'underscore';
import superagent from 'superagent';
import { createStore } from 'redux';
import { reducer } from './blueGreenModule';
//import humanize from 'tiny-human-time';
//import { Promise } from 'when';
import { colorConsole } from 'tracer';

const logger = colorConsole();

const store = createStore( reducer );
const dispatch = store.dispatch.bind( store );
const getState = store.getState.bind( store );

const LIVE_HOST = process.env.LIVE_HOST;
const STG_HOST = process.env.STG_HOST;
const CONTAINER_NAME = process.env.CONTAINER_NAME;
const BLUE_GREEN_MANAGER_NAME = `${CONTAINER_NAME}-blue-green`;
const SWITCH_NAME = `${CONTAINER_NAME}-switch`;
const DOCKER_RUN_COMMAND = process.env.DOCKER_RUN_COMMAND || '';
//const GIT_REPO = process.env.GIT_REPO;
const REGISTRY_IMAGE = process.env.REGISTRY_IMAGE;
const CONFD_PATH = path.resolve('/etc/nginx/conf.d');	// Volume from Dockerfile

const EXPOSE_PORT = 80;

export const getInfo = () => getState();
const getContainerName = (color)=> `${CONTAINER_NAME}-${color}`;
const getConfFilePath = (color)=> `${CONFD_PATH}/${color}.conf`;

export const init = () =>{
  execSync(`docker stop ${SWITCH_NAME}`);
  execSync(`docker rm ${SWITCH_NAME}`);
  execSync([
    `docker run -d --name ${SWITCH_NAME}`,
    '--restart always',
    '--expose 80',
    `--volumes-from ${BLUE_GREEN_MANAGER_NAME}`,
    `--env VIRTUAL_HOST=${LIVE_HOST},${STG_HOST}`,
    'nginx'
  ]);

  updateContainerInfoList();
  _.some( getState().colorList, color=>{
    const confFilePath = getConfFilePath( color );
    const lsResult = execSync(`ls ${confFilePath}`);
    if( lsResult.status === 0 ) {
      switchLive( color );
      return true;
    }
  });

  if( ! getState().live ) {
    switchLive( getState().colorList[0] );
  }
};

export const switchLive = ()=>{
  const { live, colorList } = getState();
  const newLive = _.find( colorList, _color=> _color !== live );
  logger.debug('begin switch to ' + newLive );
  dispatch({ type: 'CHANGE_LIVE_TO', color: newLive });
  updateNginxConf();
};

const updateNginxConf = ()=>{

  const content = getConf();

  _.each( getState().colorList, color=>{
    const confFilePath = getConfFilePath( color );
    execSync(`rm ${confFilePath}`);
  });

  try {
    const newConfFilePath = getConfFilePath( getState().live );
    fs.writeFileSync( newConfFilePath, content );
  } catch (err) {
    logger.error('can not make {color}.conf file');
    logger.error( err );
    return;
  }

  execSync(`docker exec ${SWITCH_NAME} bash -c "service nginx reload"`);
};
function getConf() {

  const { infos, live } = getState();
  const confList = _.map( infos, (info, color)=>{
    const HOST = color === live ? LIVE_HOST : STG_HOST;
    const { internalIP } = (info.containerInfo || {});
    if( ! internalIP ) {
      return '';
    }

    return (`
upstream ${HOST} {
  # Can be connect with "bridge" network
  # ${color===live ? 'LIVE' : 'STG'}
  server ${internalIP}:${EXPOSE_PORT};
}
server {
  server_name ${HOST};
  listen 80 ;
  location / {
    proxy_pass http://${HOST};
  }
}`
    );
  });

  return confList.join('\n');
}


const updateContainerInfoList = () => _.each( getState().colorList, color=>{

  const containerName = getContainerName( color );
  const result = execSync(`docker inspect ${containerName}`, { silent: true });
  if( result.status !== 0 ) {
    logger.error(`${containerName} Container 정보를 조회할수 없습니다`);
    dispatch({ type: 'UPDATE_CONTAINER_INFO', color, containerInfo: {} });
    return;
  }

  const info = JSON.parse( result.stdout )[0];
  const containerInfo = getInfoFromInspectResult( info, color );
  dispatch({ type: 'UPDATE_CONTAINER_INFO', color, containerInfo });
});

const getInfoFromInspectResult = ( inspectResult, color ) =>{

  let revisionFromEnv = _.find( inspectResult.Config.Env, keyval=>{
    const key = keyval.split("=")[0];
    return key === 'REVISION';
  });
  revisionFromEnv = revisionFromEnv ? revisionFromEnv.split("=")[1] : '';

  const cInfo = {
    color: color,
    domain: getState().live === color ? LIVE_HOST : STG_HOST,
    containerName: inspectResult.Name,
    revision: revisionFromEnv,
    internalIP: inspectResult.NetworkSettings.IPAddress,
    containerId: inspectResult.Id,
    Image: inspectResult.Image,
    State: inspectResult.State,
  };

  return cInfo;
};

const _healthCheck = () => _.each( getState().infos, info=>{

  const { color, containerInfo } = info;

  updateContainerInfoList();

  if( ! containerInfo.internalIP ) {
    logger.debug( `${color} healthCheck fail! ip 정보가 없습니다.` );
    return;
  }

  superagent.get(`http://${containerInfo.internalIP}/healthCheck`)
  .set('Accept', 'application/json')
  .end( (err, res)=>{
    const healthCheck = err ? 'BAD' : res.body.status ? 'GOOD' : 'BAD';
    const healthCheckInfo = err ? {} : res.body.status ? res.body : {};

    dispatch({ type: 'UPDATE_HEALTH_INFO', color, healthCheck, healthCheckInfo });
  });
});

setInterval( _healthCheck, 7000 );

export const startServerToStg = ( ref )=>{
  // 3개 컬러 이상될땐 가장 오래된 switchTime 가진것을 선택하자

  const { status, colorList, live } = getState();
  if( status === 'deploying' ) {
    return true;
  }
  dispatch({ type: 'START_DEPLOY' });
  const color = _.find( colorList, _color=> _color !== live );
  startServer( color, ref );
};

export const startServer = ( color, revision ) =>{

  if( getState().colorList.indexOf( color ) < 0 ) {
    logger.error('Wrong color');
    return;
  }

  const containerName = getContainerName( color );

  execSync(`docker stop ${containerName}`);
  execSync(`docker rm ${containerName}`);
  execSync([
    `docker run --name ${containerName} -d`,
    `--env REVISION=${revision}`,
    '--expose 80',
    `${REGISTRY_IMAGE}:${revision}`,
    DOCKER_RUN_COMMAND,
  ]);

  updateContainerInfoList();
  updateNginxConf();
  dispatch({ type: 'END_DEPLOY' });
};
