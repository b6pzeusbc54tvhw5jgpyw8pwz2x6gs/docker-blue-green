
const colorInit = () =>({
  color: '',
  switchTime: '',
  healthCheck: 'BAD',
  containerInfo: null,
  healthCheckInfo: null,
});
const colorReducer = ( state=colorInit(), action )=>{

  switch( action.type ) {
  case 'UPDATE_CONTAINER_INFO':
    state = {
      ...state,
      color: action.color,
      switchTime: Date.now(),
      containerInfo: action.containerInfo,
    };
    break;

  case 'UPDATE_HEALTH_INFO':
    state = {
      ...state,
      healthCheck: action.healthCheck,
      healthCheckInfo: action.healthCheckInfo,
    };
    break;

  default:
    break;
  }

  return state;
};

const init = ()=>({
  colorList: ['blue','green'],
  live: '',
  status: 'good',
  lastSwitchTime: '',
  infos: {},
});
export const reducer = ( state=init(), action ) => {

  switch( action.type ) {
  case 'START_DEPLOY':
    state = {
      ...state,
      status: 'deploying',
    };
    break;

  case 'END_DEPLOY':
    state = {
      ...state,
      status: 'good',
    };
    break;

  case 'CHANGE_LIVE_TO':
    state = {
      ...state,
      live: action.color,
      lastSwitchTime: Date.now(),
    };
    break;

  case 'UPDATE_CONTAINER_INFO':
  case 'UPDATE_HEALTH_INFO':
    state = {
      ...state,
      infos: {
        ...state.infos,
        [action.color]: colorReducer( state.infos[action.color], action ),
      }
    };
    break;

  default:
    break;
  }

  return state;
};

export default exports;
