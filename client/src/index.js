/* globals document, window*/
import React from 'react';
import ReactDOM from 'react-dom';
import request from 'superagent';
import logger from 'loglevel';
import { Promise } from 'when';

let _sha = '';
const rootEl = document.getElementById('root');

class App extends React.Component {

	render() {

		return (
			<div>
				<pre>
					{ JSON.stringify( this.props, null, 2 )}
				</pre>

        <input ref={ node=> _sha = node } placeholder={'sha'} type='text'/>
        <button onClick={ ()=>deploy( _sha.value ) }>deployToSTG</button>
				<button onClick={ goSwitch }>goSwitch</button>
			</div>
		);
	}
}

let data = {};

const fetch = ( url, reqParam ) => new Promise( (resolve) => {

	return request.post( url )
	.send( reqParam || {})
	.set('Accept', '*/*')
	//.set('Accept', 'application/json')
	.set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
	.withCredentials()
	.end( (err,res)=>{

		if( err ) {
			console.error( err );
			return;
		}

		resolve( res );
	});
});


const deploy = ( sha )=>{
	fetch( '/deployToSTG', { sha }).then( (res) => {
		logger.debug( res );
	});
};

function goSwitch() {

	const pw = window.prompt('당신이 지금 무슨짓을 하려는지 알고 계심니까?');

	data.pending = true;
	ReactDOM.render( <App {...data}/>, rootEl );

	fetch( '/switch', { pw }).then( (res) => {

		data = res.body;
		data.pending = false;
		ReactDOM.render( <App {...data}/>, rootEl );
	});
}


function updateInfo() {

	data.pending = true;
	ReactDOM.render( <App {...data}/>, rootEl );

	fetch( '/getInfo' ).then( (res) => {
		data = res.body;
		data.pending = false;
		ReactDOM.render( <App {...data }/>, rootEl );
	});
}

updateInfo();
setInterval( updateInfo, 5000 );
