'use strict';
const path = require('path');
const webpack = require('webpack');
//const _ = require('underscore');
//const rawLoaderRootPath = encodeURIComponent('../');

const publicPath = path.join( process.cwd(), 'server', 'public' );

const config = {

	//devtool: 'inline-source-map',
	//context: path.resolve( __dirname, 'src' ),
	entry: {
		[`bundle.js`]: path.resolve( __dirname, 'src/index.js' ),
	},

	output: {
		path: publicPath,
		filename: '[name]',
	},

	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify( "production" ),
			},
			__DEVELOPMENT__: false,
			__PRODUCTION__: true,
		}),
	],

	module: {
		loaders: [{

			// for webclient/src/**/*.js
			test: new RegExp( "^"+path.resolve( __dirname,'src')+"\/.*\.js"),
			exclude: /(node_modules|bower_components)/,
			loader: 'babel',
			query: {
				presets: ['react','es2015'],
				plugins: [
					["css-in-js", {
							//minify: true,	// 같은 속성을 1개로 합쳐서 셀렉트 우선순위가 어긋난다
							compressClassNames: true,
							bundleFile: `public/bundle.css`
						}
					]
				]
			}
		}]
	},
};

exports['default'] = config;
module.exports = exports['default'];
