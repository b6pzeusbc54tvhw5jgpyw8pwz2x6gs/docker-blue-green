FROM b6pzeusbc54tvhw5jgpyw8pwz2x6gs/ubuntu16-for-gitlab-runner:d170123.1
MAINTAINER Alfred UC
LABEL Name=abc Version=0.0.0
EXPOSE 80
VOLUME ["/etc/nginx/conf.d"]
COPY . /root/app
WORKDIR /root/app

# If you met this warning message: warning You don't appear to have an internet connection. \nrefer to the https://stackoverflow.com/questions/40186104/yarn-warning-on-docker-build
RUN yarn
CMD npm start
