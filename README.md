# docker-blue-green
docker 환경에서 실행되는 프로젝트를 위한 blue-green 배포 및 모니터링 툴.
이 docker-blue-green 툴 또한 docker 환경으로 실행됨.

## docker-blue-green 실행

### 1. .env 파일작성
```
LIVE_HOST=alfreduc.com
STG_HOST=stg.alfreduc.com
CONTAINER_NAME=alfreduc

VIRTUAL_HOST=blue-green-manager.alfreduc.com
VIRTUAL_PORT=80

REGISTRY_IMAGE=registry.gitlab.com/b6pzeusbc54tvhw5jgpyw8pwz2x6gs/alfred-uc
DOCKER_RUN_COMMAND='bash -c "cd /root/server && npm install && npm run make-article-json && npm run build && npm run start:prd"'
```

- LIVE_HOST: Live zone의 domain
- STG_HOST: Staging zone의 domain
- CONTAINER_NAME: docker container name. 에를들어 myProject 라면,
  `myProject-switch` 란 reverse proxy 서버인 switch 역할을 하는 nginx 서버 container 가 실행되며,
  각 blue, green zone 을 위해, `myProject-blue`, `myProject-green` 이름으로 container 가 실행됨
- REGISTRY_IMAGE: `myProject` 의 docker image 가 있는 장소 registry path
- DOCKER_RUN_COMMAND: `REGISTRY_IMAGE` 를 실행할때 사용할 run command.
  - 예를들어 node 프로젝트의 경우 `npm start` 가 될 수 있다.
  - 명시하지 않는 경우 docker image 가 빌드될때의 Dockerfile 에 따라 CMD 또는 ENTRY_POINT 가 실행됨
```

example:
```
LIVE_HOST=alfreduc.com
STG_HOST=stg.alfreduc.com
CONTAINER_NAME=alfreduc

REGISTRY_IMAGE=registry.gitlab.com/b6pzeusbc54tvhw5jgpyw8pwz2x6gs/alfred-uc
DOCKER_RUN_COMMAND='npm start'
```

### 2. docker-compose.yml 파일작성
```yml
version: '2'
services:
  docker-blue-green:
    image: registry.gitlab.com/b6pzeusbc54tvhw5jgpyw8pwz2x6gs/docker-blue-green:v0.1.0
    container_name: ${CONTAINER_NAME}-blue-green
    network_mode: "bridge"

    ports:
	  - 3000:3000

    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ./.docker:/root/.docker 	# if you need docker auths to access image,
	                                # 1. login docker registry in the host,
									# 2. copy auths directory(~/.docker) to current working directory,
									# 3. set volumes here.
									# Note:
									# ~/.docker/config.json file contains all your registry access tokens,
									# modify config.json file to remove unnecessary auths after copy.
	environment:
	  - LIVE_HOST=$LIVE_HOST
	  - STG_HOST=$STG_HOST
	  - CONTAINER_NAME=$CONTAINER_NAME
	  - REGISTRY_IMAGE=$REGISTRY_IMAGE
	  - DOCKER_RUN_COMMAND=$DOCKER_RUN_COMMAND
	  - PORT=3000
```

- image: 공식 gitlab repository 의 [container registry][registry] 에서 릴리즈된 버전을 확인.
- PORT: docker-blue-green 서버가 사용할 PORT.

### 3. 실행
1,2번에서 작성한 `.env` 파일과 `docker-compose.yml` 파일을 같은 디렉토리에 있는지 확인.
docker-compose 명령어로 실행.

```
$ docker-compose up
```

## myProejct 실행
1. 브라우저로 127.0.0.1:3000 접속
1. 배포할 myProject docker registry 로 배포된 tag 태그를 명시.

## debug

```
$ docker-compose -f docker-compose.debug.yml up
```

[registry]: https://gitlab.com/b6pzeusbc54tvhw5jgpyw8pwz2x6gs/docker-blue-green/container_registry
